﻿using NUnit.Framework;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using PrivateType = Microsoft.VisualStudio.TestTools.UnitTesting.PrivateType;

namespace Shoptimiza.Tests
{
    [TestFixture()]
    public class AuthHeaderTests
    {

        DateTime now = new DateTime(2018, 2, 14, 14, 51, 31, DateTimeKind.Utc);
        const string apiKey = "134654";
        const string secret = "secret";

        [Test()]
        public void UriWithoutProtocolTest()
        {
            var uri = "https://www.shoptimiza.com";

            PrivateType privateType = new PrivateType(typeof(AuthHeader));
            string retVal = (string)privateType.InvokeStatic("UriWithoutProtocol", uri);
            Assert.AreEqual("www.shoptimiza.com",retVal);
            
        }
        [Test()]
        public void DateTimeToUnixTimeStampTest()
        {
            var expected = 1533753656;
            var input = new DateTime(2018, 8, 8, 18, 40, 56, DateTimeKind.Utc);
            
            PrivateType privateType = new PrivateType(typeof(AuthHeader));
            int retVal = (int)privateType.InvokeStatic("DateTimeToUnixTimeStamp", input);
            Assert.AreEqual(expected,retVal);
        }
        
        [Test()]
        public void GetFileHashTest()
        {
            var expected = "iFAkiKNIlGsmFGR5Nm1nUnHtK8g=";
            //TODO: Sure there is a better way to get the directory...
            var filePath = Path.Combine(TestContext.CurrentContext.TestDirectory, "..", "..", "fixtures", "file-for-hashing.txt");
            
            string retVal = AuthHeader.GetFileHash(filePath);
            Assert.AreEqual(expected,retVal);
        }

        [Test()]
        public void SignatureForGetTest()
        {
            var actual = AuthHeader.SignatureForGetRequest(apiKey, now,  "https://shoptimiza.com/hola", secret);
            var expected = "134654.1518619891.SOvueGN73WAkfa6DguAZ7EEHKaMczZUsjdtKWfsK/r0=";
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void SignatureForPutTest()
        {
            var bodySignature = "iFAkiKNIlGsmFGR5Nm1nUnHtK8g=";
            var actual = AuthHeader.SignatureForPutRequest(apiKey, now,  "shoptimiza.com/hola", secret, bodySignature);
            
            var expected = "134654.1518619891.iFAkiKNIlGsmFGR5Nm1nUnHtK8g=.7NxHro28fEBXLikps0LMvHcsr47Ig8t2OPoFVmNghPw=";
            Console.WriteLine(actual);
            Assert.AreEqual(expected, actual);
        }

    }
}