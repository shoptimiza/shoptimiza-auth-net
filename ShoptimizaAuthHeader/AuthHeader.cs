﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Shoptimiza
{
    public class AuthHeader
    {
        private static readonly Encoding encoding = Encoding.UTF8;
        public static String Header = "X-Shoptimiza-Auth";
        public enum Method
        {
            GET, POST, PUT
        }

        public static int DateTimeToUnixTimeStamp(DateTime input)
        {
            var startOfTime = new DateTime(1970, 1, 1, 0, 0, 0);
            return (int)(input.ToUniversalTime() - startOfTime).TotalSeconds;

        }
        /// <summary>
        /// It removes the protocol from the url
        /// </summary>
        /// <param name="uri">An absolute uri</param>
        /// <returns>A uri without protocol</returns>
        private static string UriWithoutProtocol(string uri)
        {
            if (uri.Length == 0)
            {
                throw new ArgumentException("uri can not be an empty string");
            }
            if (uri.StartsWith("https://"))
            {
                return uri.Substring(8);
            }
            if (uri.StartsWith("http://"))
            {
                return uri.Substring(7);
            }
            if (uri.StartsWith("//"))
            {
                return uri.Substring(2);
            }
            if (uri.StartsWith("/"))
            {
                throw new ArgumentException("url can not be relative");
            }
            return uri;
        }

        public static string GetFileHash(string filePath)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            using (var sha1 = SHA1Managed.Create())
            {
                var hash = sha1.ComputeHash(fs);
                return Convert.ToBase64String(hash);
            }
        }
        private static string SignatureForNonBodyRequests(string apiKey, DateTime now, string httpVerb, string uri, string secret)
        {
            var unixTime = DateTimeToUnixTimeStamp(now);
            var clearText = apiKey + "." + unixTime + "." + httpVerb + "." + UriWithoutProtocol(uri.ToString());
            var key = encoding.GetBytes(secret);
            using (var hmacSha256 = new HMACSHA256(key))
            {
                hmacSha256.ComputeHash(encoding.GetBytes(clearText));
                return apiKey + "." + unixTime + "." + Convert.ToBase64String(hmacSha256.Hash);
            }

        }
        public static string SignatureForGetRequest(String apiKey, DateTime now, string uri, String secret)
        {
            return SignatureForNonBodyRequests(apiKey, now, "GET", uri, secret);
        }

        private static string SignatureForRequestsWithBody(string apiKey, DateTime now, string httpVerb, string uri, string secret, string bodySignature)
        {
            var unixTime = DateTimeToUnixTimeStamp(now);
            var clearText = apiKey + "." + unixTime + "." + httpVerb + "." + UriWithoutProtocol(uri.ToString()) + "." + bodySignature;
            var key = encoding.GetBytes(secret);
            using (var hmacSha256 = new HMACSHA256(key))
            {
                hmacSha256.ComputeHash(encoding.GetBytes(clearText));
                return apiKey + "." + unixTime + "." + bodySignature + "." + Convert.ToBase64String(hmacSha256.Hash);
            }

        }
        public static string SignatureForPutRequest(String apiKey, DateTime now, string uri, String secret, String bodySignature)
        {
            return SignatureForRequestsWithBody(apiKey, now, "PUT", uri, secret, bodySignature);
        }
        public static string SignatureForPostRequest(String apiKey, DateTime now, string uri, String secret, String bodySignature)
        {
            return SignatureForRequestsWithBody(apiKey, now, "POST", uri, secret, bodySignature);
        }
    }
}
